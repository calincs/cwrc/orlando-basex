# Orlando BaseX
## Commands to set after data is ingested
```php

// to precalculate number of facets for entire database, author, event, bibliographic, organization and person documents
$queryString = "import module namespace extract = 'org.basex.modules.extract';   let \$database:='orlando' let \$storingDatabase := 'mapping' return extract:extractAllFacetData(\$database, \$storingDatabase) ";
$this->session->query($queryString);

$this->session->execute("OPEN {$this->connectionOptions['database']}");
$this->session->execute('SET INTPARSE true');
$this->session->execute('SET STEMMING true');
$this->session->execute('SET STOPWORDS [filepath]');
$this->session->execute('CREATE INDEX TEXT');
$this->session->execute('CREATE INDEX ATTRIBUTE');
$this->session->execute('CREATE INDEX FULLTEXT');
```
## Search Query Structure
### Sample data
``` xquery
import module namespace search = 'org.basex.modules.search'; 

let $database := "orlando" 
let $searchTerms := ("Virginia", "Woolf)
let $filters := map{"result":("event"), "period":("1066-12-25_1501", "woolvi", "1936-12-10_1952-02-06"), "gender":("woman"), "date":map{"start":"1730-01-01", "end":"1940-01-01"}, "tagIncluded":("TITLE"), "tagExcluded":("ETHNICITY"), "locationIncluded":("america"), "locationExcluded":("russia")}
let $sorting := "alphabetical"
let $direction := "descending"
let $advanced := map{}
let $stemming := "true"
return search:getCompleteSearchResults($database, $searchTerms, $filters, $sorting, $direction, $stemming)
(:
return local:getCompleteAdvancedSearchResults($database, $searchTerms, $filters, $sorting, $direction,  $advanced, $stemming)
:)
```

### Filter terms
Keys are: "result", "gender", "genre", "nationality", "period", "date", "tagIncluded", "tagExcluded"
All except for "date" have xpath sequences as values.

Note that since the result type can have a subgroup type, instead of the result filter using a list of strings it uses a list of maps. Each map has the value <strong>name</strong> (result type) followed by <strong>sub_group</strong> (can be left as blank string for results that are not events, otherwise the possible values are described in the event_mapping.xml file)
### Sorting
Sorting can be "alphabetical", "chronological" or "relevance"
Direction can be "ascending" or "descending"
Stemming can be "true" or "false"

## Getting Base facets
### To generate base facets (done after the data ingestion)
#### Assumes that you will use the mapping database to also store this information
For all:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractBaseFacetData($database, $storingDatabase)
```
For author entries:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractAuthorFacetData($database, $storingDatabase)
```
For events:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractEventFacetData($database, $storingDatabase)
```
For bibliographic entities:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractTitleFacetData($database, $storingDatabase)
```
For organization entities:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractBaseOrganizationData($database, $storingDatabase)
```
For person entities:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando"
let $storingDatabase := "mapping"
return extract:extractPersonFacetData($database, $storingDatabase)
```
#### To run all of the above at once:
``` xquery
import module namespace extract = 'org.basex.modules.extract'; 
let $database := "orlando" 
let $storingDatabase := "mapping"
return extract:extractAllFacetData($database, $storingDatabase)
```
### Sample data
``` xquery
import module namespace search = 'org.basex.modules.search'; 
let $storingDatabase := "mapping"
let $type := "author"
return search:getCalculatedFacetData($storingDatabase, $type)
```
### Type terms
Type can be "base", "author", "event", "title", "organization" or "person"

