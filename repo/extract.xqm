
(:
-------------EXTRACTION MODULE-----------
This module will pull data about facets from each document and add a new FACET tag at the start of the doc containing the relevant information
------------------------------------------
:)

module namespace extract = 'org.basex.modules.extract';

import module namespace helper = "org.basex.modules.helper";
import module namespace date = "org.basex.modules.date";
import module namespace search = "org.basex.modules.search";
import module namespace db = "http://basex.org/modules/db";


declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace xlink = "http://www.w3.org/1999/xlink";
declare namespace orl = "http://ualberta.ca/orlando";
declare namespace mods = "http://www.loc.gov/mods/v3";

(:
Update a database file by inserting an extracted <facet> element at the beginning, contents depending on document type
Delete un-needed XML elements before doing the extraction 
:)
declare %public updating function extract:extractFacetsForDatabase($uri) {
  
    try{
        if(helper:getResultType($uri) = "author") then
            (extract:processAuthorDelete($uri),  extract:processAuthorFacets($uri))
        else if (helper:getResultType($uri) = "event") then
            (extract:processEventDelete($uri), extract:processEventFacets($uri))
        else if (helper:getResultType($uri) = "embeddedEvent") then
            (extract:processEventDelete($uri), extract:processEmbeddedEventFacets($uri))  
        else if (helper:getResultType($uri) = "title") then
            (extract:processTitleDelete($uri), extract:processTitleFacets($uri)) 
                else if (helper:getResultType($uri) = "organization") then
            (extract:processOrganizationDelete($uri), extract:processOrganizationFacets($uri))  
                    else if (helper:getResultType($uri) = "person") then
            (extract:processPersonDelete($uri), extract:processPersonFacets($uri))  
          } catch * {
              error(xs:QName('failed_injestion'), 'Could not extract facets from ' || $uri || '[' || $err:code || ']: ' || $err:description)
          } 
};



(: ----------------------------------------------
Main extraction functions for each document type
------------------------------------------------:)

(:
For a document of result type author entry: 
Extract related document uri, result type, gender, nationality, genre, birth, death, periods, locations
Update the document with a new facet element at the top
:)  
declare %private updating function extract:processAuthorFacets($uri) {
            
    let $hit := doc($uri)
    let $resultType := "author"
    let $documentName := $hit//DOCTITLE/text()
    let $gender := extract:getGenderFacetCount($uri)
    let $nationality := extract:getNationalityFacetCount($uri)
    let $genre := extract:getGenreFacetCount($uri)
    let $birthDate := date:getLowerDateRange(date:getBirthEstimate($uri))
    let $deathDate := date:getUpperDateRange(date:getDeathEstimate($uri))
    let $periods := extract:getTimePeriodFacetCount($birthDate,$deathDate)
    let $monarchPeriods := extract:getMonarchPeriodFacetCount($birthDate,$deathDate)
    let $authorPeriods := extract:getAuthorPeriodFacetCount($birthDate,$deathDate)
    let $locations := extract:getLocationFacetCount($uri)



        return insert node
            <facet>
                <uri>{$uri}</uri>
                <doc-name>{$documentName}</doc-name>
                <facet-result>{$resultType}</facet-result>
                {$gender}
                {$nationality}
                {$genre}
                {$periods}
                {$monarchPeriods}
                {$authorPeriods}
                {$locations}
                <facet-start>{$birthDate}</facet-start>
                <facet-end>{$deathDate}</facet-end>
            </facet>
            as first into $hit  

 };
 
 
(:
For documents of result type event:
Extract start date, end date, and periods
Update the document with a new facet element at the start
:)   
 declare %private updating function extract:processEventFacets($uri) {
    
    let $hit := doc($uri)
    let $resultType := "event"
    let $documentName := $hit//DOCTITLE/text()

    return 
        if (exists($hit//CHRONSTRUCT//DATE/@VALUE)) then
                let $date := $hit//CHRONSTRUCT//DATE/@VALUE/string()
                let $startDate :=  date:getLowerDateRange($date[1])
                let $endDate :=  date:getUpperDateRange($date[1])
                let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
                let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
                let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
                let $eventSource := extract:getEventSource($resultType)
                let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
                let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
            
        else if (exists($hit//CHRONSTRUCT//DATERANGE)) then
            let $startDate :=  date:getLowerDateRange($hit//CHRONSTRUCT//DATERANGE/@FROM/string())[1]
            let $endDate :=  date:getUpperDateRange($hit//CHRONSTRUCT//DATERANGE/@TO/string())[1]
            let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $eventSource := extract:getEventSource($resultType)
            let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
            let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())

                
                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
                
        else if (exists($hit//CHRONSTRUCT//DATESTRUCT/@VALUE)) then
            let $date := $hit//CHRONSTRUCT//DATESTRUCT/@VALUE/string()
            let $startDate :=  date:getLowerDateRange($date[1])
            let $endDate :=  date:getUpperDateRange($date[1])
            let $periods := extract:getTimePeriodFacetCount($startDate, $endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $eventSource := extract:getEventSource($resultType)
            let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
            let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit
            else if (exists($hit//SHORTPROSE//DATE/@VALUE)) then
                let $date := $hit//SHORTPROSE//DATE/@VALUE/string()
                let $startDate :=  date:getLowerDateRange($date[1])
                let $endDate :=  date:getUpperDateRange($date[1])
                let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
                let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
                let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
                let $eventSource := extract:getEventSource($resultType)

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
            
        else if (exists($hit//SHORTPROSE//DATERANGE)) then
            let $startDate :=  date:getLowerDateRange($hit//SHORTPROSE//DATERANGE/@FROM/string())[1]
            let $endDate :=  date:getUpperDateRange($hit//SHORTPROSE//DATERANGE/@TO/string())[1]
            let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $eventSource := extract:getEventSource($resultType)

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
                
        else if (exists($hit//SHORTPROSE//DATESTRUCT)) then
            let $date := $hit//SHORTPROSE//DATESTRUCT/@VALUE/string()
            let $startDate :=  date:getLowerDateRange($date[1])
            let $endDate :=  date:getUpperDateRange($date[1])
            let $periods := extract:getTimePeriodFacetCount($startDate, $endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $eventSource := extract:getEventSource($resultType)

                
                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit
            else
              error(xs:QName('dateless_event'), "This event document did not contain a DATE, DATERANGE, or DATESTRUCT element in the CHRONSTRUCT or SHORTPROSE")
                    
};

(:
For documents of result type embeddedEvent:
Extract related document uri, result type, gender, nationality, genre, birth, death, periods, locations
Update the document with a new facet element at the start
:)   
declare %private updating function extract:processEmbeddedEventFacets($uri) {
    
    let $hit := doc($uri)
    let $resultType := "embeddedEvent"
    let $documentName := $hit//CHRONSTRUCT/@id/string()

return 
        if (exists($hit//CHRONSTRUCT//DATE/@VALUE)) then
                let $date := $hit//CHRONSTRUCT//DATE/@VALUE/string()
                let $startDate :=  date:getLowerDateRange($date[1])
                let $endDate :=  date:getUpperDateRange($date[1])
                let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
                let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
                let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
                let $genre := extract:getGenreFacetCount($uri)
                let $locations := extract:getLocationFacetCount($uri)
                let $gender := extract:getGenderFacetCount($uri)
                let $nationality := extract:getNationalityFacetCount($uri)
                let $eventSource := extract:getEventSource($resultType)
                let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
                let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())   

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
            
        else if (exists($hit//CHRONSTRUCT//DATERANGE)) then
            let $dateRange := ($hit//CHRONSTRUCT//DATERANGE)[1]
            let $startDate :=  date:getLowerDateRange($dateRange/@FROM/string())
            let $endDate :=  date:getUpperDateRange($dateRange/@TO/string())            
            let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $genre := extract:getGenreFacetCount($uri)
            let $locations := extract:getLocationFacetCount($uri)
            let $gender := extract:getGenderFacetCount($uri)
            let $nationality := extract:getNationalityFacetCount($uri)
            let $eventSource := extract:getEventSource($resultType)
            let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
            let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())   

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
                
        else if (exists($hit//CHRONSTRUCT//DATESTRUCT/@VALUE)) then
            let $date := $hit//CHRONSTRUCT//DATESTRUCT/@VALUE/string()
            let $startDate :=  date:getLowerDateRange($date[1])
            let $endDate :=  date:getUpperDateRange($date[1])
            let $periods := extract:getTimePeriodFacetCount($startDate, $endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $genre := extract:getGenreFacetCount($uri)
            let $locations := extract:getLocationFacetCount($uri)
            let $gender := extract:getGenderFacetCount($uri)
            let $nationality := extract:getNationalityFacetCount($uri)
            let $eventSource := extract:getEventSource($resultType)
            let $eventType := extract:getEventType($hit//CHRONSTRUCT/@CHRONCOLUMN/string())
            let $eventSignificance := extract:getEventSignificance($hit//CHRONSTRUCT/@RELEVANCE/string())   


                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        <facet-event_type>{$eventType}</facet-event_type>
                        <facet-event_significance>{$eventSignificance}</facet-event_significance>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit
            else if (exists($hit//SHORTPROSE//DATE/@VALUE)) then
                let $date := $hit//SHORTPROSE//DATE/@VALUE/string()
                let $startDate :=  date:getLowerDateRange($date[1])
                let $endDate :=  date:getUpperDateRange($date[1])
                let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
                let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
                let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
                let $genre := extract:getGenreFacetCount($uri)
                let $locations := extract:getLocationFacetCount($uri)
                let $gender := extract:getGenderFacetCount($uri)
                let $nationality := extract:getNationalityFacetCount($uri)
                let $eventSource := extract:getEventSource($resultType)

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
            
        else if (exists($hit//SHORTPROSE//DATERANGE)) then
            let $startDate :=  date:getLowerDateRange($hit//SHORTPROSE//DATERANGE/@FROM/string())[1]
            let $endDate :=  date:getUpperDateRange($hit//SHORTPROSE//DATERANGE/@TO/string())[1]
            let $periods := extract:getTimePeriodFacetCount($startDate,$endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $genre := extract:getGenreFacetCount($uri)
            let $locations := extract:getLocationFacetCount($uri)
            let $gender := extract:getGenderFacetCount($uri)
            let $nationality := extract:getNationalityFacetCount($uri)
            let $eventSource := extract:getEventSource($resultType)

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
                
        else if (exists($hit//SHORTPROSE//DATESTRUCT)) then
            let $date := $hit//SHORTPROSE//DATESTRUCT/@VALUE/string()
            let $startDate :=  date:getLowerDateRange($date[1])
            let $endDate :=  date:getUpperDateRange($date[1])
            let $periods := extract:getTimePeriodFacetCount($startDate, $endDate) 
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)
            let $genre := extract:getGenreFacetCount($uri)
            let $locations := extract:getLocationFacetCount($uri)
            let $gender := extract:getGenderFacetCount($uri)
            let $nationality := extract:getNationalityFacetCount($uri)
            let $eventSource := extract:getEventSource($resultType)

                return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{"event"}</facet-result>
                        <facet-event_source>{$eventSource}</facet-event_source>
                        {$gender}
                        {$nationality}
                        {$genre}
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        {$locations}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit
            else
              error(xs:QName('dateless_event'), "This event document did not contain a DATE, DATERANGE, or DATESTRUCT element in the CHRONSTRUCT or SHORTPROSE")
                    
};

(:
For documents of result type bibliography:
Extract start date, end date, and periods
Update the document with a new facet element at the start
:)   
 declare %private updating function extract:processTitleFacets($uri) {
   
    let $hit := doc($uri)
    let $resultType := "title"
    let $documentName := $hit//mods:titleInfo[1]/mods:title/text()

    return 
        if (exists($hit//mods:originInfo//mods:dateIssued[@encoding="iso8601"])) then
            let $date := $hit//mods:originInfo//mods:dateIssued[@encoding="iso8601"]/string()
            let $startDate :=  date:getUpperDateRange($date[1])
            let $endDate :=  date:getUpperDateRange($date[1])
            let $periods := extract:getTimePeriodFacetCount($startDate,$endDate)
            let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
            let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)


                    return insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                        {$periods}
                        {$monarchPeriods}
                        {$authorPeriods}
                        <facet-start>{$startDate}</facet-start>
                        <facet-end>{$endDate}</facet-end>
                    </facet> 
                    as first into $hit 
                    
            else
            insert node
                    <facet>
                        <uri>{$uri}</uri>
                        <doc-name>{$documentName}</doc-name>
                        <facet-result>{$resultType}</facet-result>
                    </facet> 
                    as first into $hit 
                   
};


(:
For documents of result type organization:
Extract document uri and result type
Update the document with a new facet element at the start
:)   
declare %private updating function extract:processOrganizationFacets($uri) {

        let $hit := doc($uri)
        let $resultType := "organization"
        let $documentName := $hit//identity//namePart[../variantType/text() = "orlandoStandardName"]//text()
        return insert node
            <facet>
                <uri>{$uri}</uri>
                <doc-name>{$documentName}</doc-name>
                <facet-result>{$resultType}</facet-result>
            </facet> 
            as first into $hit 

};

(:
For documents of result type person:
Extract uri, result type, birth, death, periods
Update the document with a new facet element at the start
:)   
 declare %private updating function extract:processPersonFacets($uri) {

        let $hit := doc($uri)
        let $resultType := "person"
        let $documentName := $hit//identity//namePart[../variantType/text() = "orlandoStandardName"]//text()

        return if (boolean($hit/entity/person//dateSingle/standardDate/text()) and count($hit//dateSingle/standardDate/string()) = 2) then 
                        let $birthDate := $hit//dateSingle/standardDate/string()
                        let $startDate := date:getLowerDateRange($birthDate[1])
                        let $deathDate := $hit//dateSingle/standardDate/string()
                        let $endDate := date:getLowerDateRange($deathDate[2])
                        let $periods := extract:getTimePeriodFacetCount($startDate,$endDate)
                        let $monarchPeriods := extract:getMonarchPeriodFacetCount($startDate,$endDate)
                        let $authorPeriods := extract:getAuthorPeriodFacetCount($startDate,$endDate)

                            return insert node
                                <facet>
                                    <uri>{$uri}</uri>
                                    <doc-name>{$documentName}</doc-name>
                                    <facet-result>{$resultType}</facet-result>
                                    {$periods}
                                    {$monarchPeriods}
                                    {$authorPeriods}
                                    <facet-start>{$startDate}</facet-start>
                                    <facet-end>{$endDate}</facet-end>
                                </facet> 
                                as first into $hit 
                         else 
                          insert node
                                <facet>
                                    <uri>{$uri}</uri>
                                    <doc-name>{$documentName}</doc-name>
                                    <facet-result>{$resultType}</facet-result>
                                </facet> 
                                as first into $hit 
};

(: ----------------------------------------------
Functions to pre-process documents by deleting un-needed xml elements
------------------------------------------------:)

(:
Delete nodes that we don't want to be searchable from author documents
:)
declare  %private updating function extract:processAuthorDelete($uri) {
      
      let $hit := doc($uri)
      return delete node $hit//ORLANDOHEADER//PUBLICATIONSTMT | $hit//ORLANDOHEADER//SOURCEDESC | $hit//ORLANDOHEADER//REVISIONDESC | $hit//DIV0/WORKSCITED  | $hit//KEYWORDCLASS

};

(:
Delete nodes that we don't want to be searchable from event documents
:)
declare  %private updating function extract:processEventDelete($uri) {
  
      let $hit := doc($uri)
      return delete node $hit//ORLANDOHEADER//PUBLICATIONSTMT | $hit//ORLANDOHEADER//SOURCEDESC | $hit//ORLANDOHEADER//REVISIONDESC | $hit//KEYWORDCLASS | $hit//SOURCES
      
};

(:
Delete nodes that we don't want to be searchable from title documents
:)
declare  %private updating function extract:processTitleDelete($uri) {
  
      let $hit := doc($uri)
      return delete node $hit//mods:accessCondition | $hit//mods:recordInfo
      
};

(:
Delete nodes that we don't want to be searchable from organization documents
:)
declare  %private updating function extract:processOrganizationDelete($uri) {
  
      let $hit := doc($uri)
      return delete node $hit//projectId | $hit//variantType[contains(.,'orlandoStandardName')]
      
};

(:
Delete nodes that we don't want to be searchable from person documents
:)
declare %private updating function extract:processPersonDelete($uri){
        let $hit := doc($uri)
      return delete node $hit//projectId | $hit//variantType[contains(.,'orlandoStandardName')]
};

(: ----------------------------------------------
Functions that will find all facet values associated with a document based on facet type
Checks facets to make sure they exist in the mapping documents
Will return results of type <facet-$name>{$facetValue}</facet-$name>
------------------------------------------------:)

(:
Find the gender associated with a given document (for extraction)
:)
declare %private function extract:getGenderFacetCount($uri) {
    
   for $gender in doc($uri)//CULTURALFORMATION//GENDER/@GENDERIDENTITY  | doc($uri)//GENDER_FACET//GENDER/@GENDERIDENTITY
        for $genderFacet in db:open("mapping")//mapping
            where (exists($genderFacet/@category = "gender") and $genderFacet//name/string() = $gender/string()[1])
                let $genderCategory := $genderFacet//machineName/string()
                    return <facet-gender>{$genderCategory}</facet-gender>                                    

};

(:
Find all the nationality tags that occur in a given document (for extraction)
:)
declare %private function extract:getNationalityFacetCount($uri)  {


    for $nationality in doc($uri)//NATIONALITY | doc($uri)//NATIONALHERITAGE 
        group by $nationalityTypes := $nationality/@REG
            for $nationalityType in $nationalityTypes
                for $nationalityFacet in db:open("mapping")//mapping
                    where (exists($nationalityFacet/@category = "nationality") and $nationalityFacet//name/string() = $nationalityType)
                        let $nationalityCategory := $nationalityFacet//machineName/string()
                            return <facet-nationality>{ $nationalityCategory}</facet-nationality>
    

};

(:
Find all the genre tags that occur in a given document (for extraction)
Takes a different approach for author entries and bilbiography entries
Returns the combined results
:)
declare %private function extract:getGenreFacetCount($uri)   {
    
     let $entryGenre := for $genre in doc($uri)//TGENRE
        group by $genreTypes := $genre/@GENRENAME 
        for $genreType in $genreTypes
        for $genreFacet in db:open("mapping")//mapping
            where (exists($genreFacet/@category = "genre") and $genreFacet//name/string() = $genreType)
                let $genreCategory := $genreFacet//machineName/string()
            return <facet-genre>{ $genreCategory }</facet-genre>

        let $biblGenre := for $genre in doc($uri)//mods:genre
        group by $genreTypes := $genre
        for $genreType in $genreTypes
          for $genreFacet in db:open("mapping")//mapping
            where (exists($genreFacet/@category = "genre") and $genreFacet//name/string() = $genreType)
                let $genreCategory := $genreFacet//machineName/string()
            return <facet-genre>{ $genreCategory }</facet-genre>

    return $entryGenre union  $biblGenre
            
};

(:
Find the time periods associated with a given document (for extraction)
:)
declare %private function extract:getTimePeriodFacetCount($startDate, $endDate) {
    
    for $timePeriod in db:open("mapping")//mapping[@category="period"]
        let $startPeriod := date:getLowerDateRange($timePeriod/date/start/text())
        let $endPeriod := date:getUpperDateRange($timePeriod/date/end/text())
        where ((xs:date($startDate) ge xs:date($startPeriod)  and xs:date($startDate) le xs:date($endPeriod)) 
  	            or (xs:date($endDate) ge xs:date($startPeriod)  and xs:date($endDate) le xs:date($endPeriod)) 
  	            or (xs:date($startDate) le xs:date($startPeriod)  and xs:date($endDate) ge xs:date($endPeriod)))
                    return <facet-period>{ $timePeriod/machineName/text()}</facet-period>

};

(:
Find the monarch lifespan periods associated with a given document (for extraction)
:)
declare %private function extract:getMonarchPeriodFacetCount($startDate, $endDate) {
    for $timePeriod in db:open("mapping")//mapping[@category="monarch"]
        let $startPeriod := date:getLowerDateRange($timePeriod/date/start/text())
        let $endPeriod := date:getUpperDateRange($timePeriod/date/end/text())
        where  ((xs:date($startDate) ge xs:date($startPeriod)  and xs:date($startDate) le xs:date($endPeriod)) 
  	            or (xs:date($endDate) ge xs:date($startPeriod)  and xs:date($endDate) le xs:date($endPeriod)) 
  	            or (xs:date($startDate) le xs:date($startPeriod)  and xs:date($endDate) ge xs:date($endPeriod)))
                    return <facet-monarch_period>{ $timePeriod/machineName/text() }</facet-monarch_period>
};

(:
Find the author lifespan periods associated with a given document (for extraction)
:)
declare %private function extract:getAuthorPeriodFacetCount($startDate, $endDate){
   for $timePeriod in db:open("mapping")//mapping[@category="author"]
        let $startPeriod := date:getLowerDateRange($timePeriod/date/start/text())
        let $endPeriod := date:getUpperDateRange($timePeriod/date/end/text())
   where  ((xs:date($startDate) ge xs:date($startPeriod)  and xs:date($startDate) le xs:date($endPeriod)) 
  	            or (xs:date($endDate) ge xs:date($startPeriod)  and xs:date($endDate) le xs:date($endPeriod)) 
  	            or (xs:date($startDate) le xs:date($startPeriod)  and xs:date($endDate) ge xs:date($endPeriod)))
                    return <facet-author_period>{ $timePeriod/machineName/text() }<subgroup></subgroup></facet-author_period>
};
(: 
Find the locations associated with a given document (for extraction)
Treat addresses, place with REG attribute and general orlando places differently
Return the combined results
:)
declare %private function extract:getLocationFacetCount($uri)  {   
    
 
   let $placeAddresses := 
        for $address in doc($uri)//PLACE//ADDRESS 
            return 
                if(exists($address[@REG])) then (
                    $address/@REG/string()
                ) else (
                    $address/ADDRLINE/string())
                    
    let $placesWithRegAttribute := 
        for $place in doc($uri)//PLACE/PLACENAME  | doc($uri)//PLACE/SETTLEMENT | doc($uri)//PLACE/REGION | doc($uri)//PLACE/GEOG | doc($uri)//PLACE/AREA 
            return $place/@REG/string()

    let $placeOrlandoNames := 
        for $place in doc($uri)//PLACE
            
            let $placeName :=
                for $placeName in $place//PLACENAME
                    return
                        if(exists($placeName[@REG])) then (
                            $placeName/@REG/string()
                        ) else if(exists($placeName/text())) then ((
                            $placeName/string()))
            
            let $address :=
                for $address in $place//ADDRESS//ADDRLINE
                    return $address/string()
            
            let $settlement := 
                for $settlement in $place//SETTLEMENT
                    return
                        if(exists($settlement[@REG])) then (
                            $settlement/@REG/string()
                        ) else if(exists($settlement/text())) then (
                             $settlement/string())
            
            let $region :=
                for $region in $place//REGION
                    return
                        if(exists($region[@REG])) then (
                            $region/@REG/string()
                        ) else if(exists($region/text())) then (
                            $region/string())
            
            let $geog :=
                    for $geog in $place//GEOG
                        return
                            if(exists($geog[@REG])) then (
                                $geog/@REG/string()
                            ) else if(exists($geog/text())) then (
                                $geog/string())
            
            let $area :=
                    for $area in $place//AREA
                        return 
                            if(exists($area[@REG])) then (
                                $area/@REG/string()
                            )  else if(exists($area/text())) then (
                                $area/string())

            return string-join(($placeName, $address, $settlement, $region, $geog, $area), ", ")
            
    for $place in   (distinct-values($placesWithRegAttribute), distinct-values($placeAddresses), distinct-values($placeOrlandoNames))
    for $placeFacet in db:open("mapping")//mapping[@category="location"]/names/name[string()=$place]
          return <facet-location>{$placeFacet/ancestor::mapping/machineName/string()}</facet-location>

};


(:
Find the machine name of a given event source
:)
declare %private function extract:getEventSource($source) {
     for $eventSourceFacet in db:open("mapping")//mapping[@category="event_source"]/names/name[string()=$source]
          return $eventSourceFacet/ancestor::mapping/machineName/string()

    
};

(:
Find the machine name of a given event type
:)
declare %private function extract:getEventType($type) {
     for $eventTypeFacet in db:open("mapping")//mapping[@category="event"]/names/name[string()=$type]
          return $eventTypeFacet/ancestor::mapping/machineName/string()

    
};


(:
Find the machine name for event significance
:)
declare %private function extract:getEventSignificance($significance) {
     for $eventSignificanceFacet in db:open("mapping")//mapping[@category="event_significance"]/names/name[string()=$significance]
          return $eventSignificanceFacet/ancestor::mapping/machineName/string()

    
};