            

(:
---------------SEARCH MODULE-------------
This is the module that contains search functions
It gets the search hits, groups them by document, and then counts facets
------------------------------------------
:)

module namespace search = 'org.basex.modules.search';

import module namespace helper = "org.basex.modules.helper";
import module namespace facet = "org.basex.modules.facet";
import module namespace ft = 'http://basex.org/modules/ft';
import module namespace db = "http://basex.org/modules/db";
import module namespace date = "org.basex.modules.date";
import module namespace functx='http://www.functx.com';
import module namespace map = 'http://www.w3.org/2005/xpath-functions/map';


declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace xlink = "http://www.w3.org/1999/xlink";
declare namespace orl = "http://ualberta.ca/orlando";
declare namespace mods = "http://www.loc.gov/mods/v3";

(:
Entry point for the search
Wrapper function for getSearchResults, allowing query to be sent without advanced data
:)
declare function search:getCompleteSearchResults($database, $searchTerms, $filters, $sorting, $direction, $stemming) {
  let $advanced := map{}
  let $results := search:getSearchResults($database, $searchTerms, $filters, $sorting, $direction, $advanced, $stemming)
  return $results
};

(:
Entry point for the search
Wrapper function for getSearchResults, allowing query to be sent with advanced data
:)
declare function search:getCompleteAdvancedSearchResults($database, $searchTerms, $filters, $sorting, $direction, $advanced, $stemming) {
    let $results := search:getSearchResults($database, $searchTerms, $filters, $sorting, $direction, $advanced, $stemming)
  return $results
};

(:
Gets hits based on search results if $searchTerms contains values, otherwise filters down from all database documents
Gets embeddedEvent facet count seperately (will be empty if embedded events are included in general results)
Across all returned documents, count the number of facets
Return facet count and documents
Sort alphabetically, chronologically or by relevance
:)
declare function search:getSearchResults($database, $searchTerms, $filters, $sorting, $direction,  $advanced, $stemming){
 
  if (exists($searchTerms)) then (
    let $documents := search:getSearchHits($database, $searchTerms, $filters,  $advanced, $stemming)//main/*
      return <results><facets>
        {                   
        for $doc in $documents
              let $facets := $doc/facets
                  for $facet in $facets/facet
                      let $type := $facet/type
                      let $name := $facet/name
                      group by $name, $type
                          return 
      
                              <facet>
                                  <type>{$type}</type>
                                  <name>{$name}</name>
                                  <count>{sum(count($doc))}</count>
                              </facet>
                        
                        
      } </facets>
      <documents>

      {
      if ($sorting = "alphabetical") then (
          if ($direction = "ascending") then (
            for $document in $documents
            let $name := $document/@documentName
                      order by $name ascending
                          return functx:remove-elements($document, "facets")
                  )
          else if ($direction= "descending") then (
              for $document in $documents
                let $name := $document/@documentName
                      order by $name descending
                          return functx:remove-elements($document, "facets")
                        )
                      )

      else if ($sorting = "chronological") then (
          if ($direction= "ascending") then (
            for $document in $documents
              let $date := $document/@start
              order by xs:date($date) ascending
                return functx:remove-elements($document, "facets")
                )
          else if ($direction= "descending") then (
            for $document in $documents
              let $date := $document/@end
              order by xs:date($date) descending
                return functx:remove-elements($document, "facets"))
                )

      else if ($sorting = "relevance") then (
          if ($direction= "ascending") then (
            for $document in $documents
              let $resultRank := if ($document/@documentType = "author") then 0 else if ($document/@documentType = "embeddedEvent") then 1 else if ($document/@documentType = "event") then 2 else if ($document/@documentType = "organization" or  $document/@documentType = "person") then 3 else if ( $document/@documentType = "title") then 4 else if ($document/@documentType = "null") then 5 
              let $titleRank := if ( $document/@documentName contains text {$searchTerms} any using stemming) then 0 else 1 
              let $hitRank := xs:integer($document/@count) 
              order by $resultRank descending, $titleRank descending, $hitRank ascending
                return functx:remove-elements($document, "facets") 
                )

          else if ($direction= "descending") then (
            for $document in $documents
              let $resultRank := if ($document/@documentType = "author") then 0 else if ($document/@documentType = "embeddedEvent") then 1 else if ($document/@documentType = "event") then 2 else if ($document/@documentType = "organization" or  $document/@documentType = "person") then 3 else if ( $document/@documentType = "title") then 4 else if ($document/@documentType = "null") then 5 
              let $titleRank := if ( $document/@documentName contains text {$searchTerms} any using stemming) then 0 else 1 
              let $hitRank := xs:integer($document/@count) 
              order by $resultRank ascending, $titleRank ascending, $hitRank descending
                return functx:remove-elements($document, "facets") 
                )
  
                      )}
      </documents>
      </results>)
  else (
    let $documents := search:getDatabaseHits($database,  $filters, $advanced)//main/*
    return 
      <results>
        <facets>
        {
          for $doc in $documents
                let $facets := $doc/facets
                    for $facet in $facets/facet
                        let $type := $facet/type
                        let $name := $facet/name
                        group by $name, $type
                            return 

                                <facet>
                                    <type>{$type}</type>
                                    <name>{$name}</name>
                                    <count>{sum(count($doc))}</count>
                                </facet>
        }                  
                    </facets>
        <documents>

            {
      if ($sorting = "alphabetical") then (
          if ($direction = "ascending") then (
            for $document in $documents
            let $name := $document/@documentName
                      order by $name ascending
                          return functx:remove-elements($document, "facets")
                  )
          else if ($direction= "descending") then (
              for $document in $documents
                let $name := $document/@documentName
                      order by $name descending
                          return functx:remove-elements($document, "facets")
                        )
                      )
      else if ($sorting = "chronological") then (
            if ($direction= "ascending") then (
  
              for $document in $documents
            
                  let $date := $document/@start
                  order by xs:date($date) ascending
                    return functx:remove-elements($document, "facets"))
                
            else if ($direction= "descending") then(
                        for $document in $documents
                  let $date := $document/@end
                  order by xs:date($date) descending
                    return functx:remove-elements($document, "facets"))
                    )
      else if ($sorting = "relevance") then (
                  if ($direction= "ascending") then (
                    for $document in $documents
                      let $resultRank := if ($document/@documentType = "author") then 0 else if ($document/@documentType = "embeddedEvent") then 1 else if ($document/@documentType = "event") then 2 else if ($document/@documentType = "organization" or  $document/@documentType = "person") then 3 else if ( $document/@documentType = "title") then 4 else if ($document/@documentType = "null") then 5 
                      let $hitRank := xs:integer($document/@count) 
                      order by  $resultRank descending,   $hitRank ascending
                        return functx:remove-elements($document, "facets") 
                        )

                  else if ($direction= "descending") then (
                    for $document in $documents
                      let $resultRank := if ($document/@documentType = "author") then 0 else if ($document/@documentType = "embeddedEvent") then 1 else if ($document/@documentType = "event") then 2 else if ($document/@documentType = "organization" or  $document/@documentType = "person") then 3 else if ( $document/@documentType = "title") then 4 else if ($document/@documentType = "null") then 5 
                      let $hitRank := xs:integer($document/@count) 
                      order by  $resultRank ascending, $hitRank descending
                        return functx:remove-elements($document, "facets") 
                        )
            )
     
                      }
            </documents>       
    </results>   ) 

  
};


(:
Gets a list of hits that contain the given search term
Filters hits by the faceting information provided in the query
Each hit will also include all associated document facets, the URI, xpath to the location of the hit, and some context
:)
declare function search:getSearchHits($database, $searchTerms, $filters,  $advanced, $stemming)  {
  
    (:If $advanced is not empty, use advanced search, otherwise use regular search:)

  let $hits := 
     if ((map:size($advanced) = 0)) then (
      for $hit in search:standardSearch($database, $searchTerms, $stemming)
        let $facets :=  fn:root($hit)/facet[1]
          where(not($hit/ancestor::facet) and exists(facet:filter($facets, $filters, $database, $hit)))
            return $hit
    ) else (
      for $hit in search:advancedSearch($database, $searchTerms, $advanced, $stemming)
        let $facets :=  fn:root($hit)/facet[1]
          where(not($hit/ancestor::facet) and exists(facet:filter($facets, $filters, $database, $hit)))
            return $hit    
    )
        
  let $allResults :=

    (:Only return embedded events in results if its explicitly selected in the filters when author entries are not:)

      for $hit in $hits
        let $facets :=  fn:root($hit)/facet[1]
        let $uri := $facets/uri
        let $documentName := $facets/doc-name/text()
        let $documentType := $facets/facet-result/text()
        let $startDate := date:getStartSortingDate($facets)
        let $endDate := date:getEndSortingDate($facets)
                
        group by $uri, $documentName, $documentType, $startDate, $endDate 
          return 
            <document uri="{$uri}" count="{count($hit)}" documentName="{$documentName}" documentType="{$documentType}" start="{$startDate}" end="{$endDate}"> 
              <facets>
                {
                  for $facet in $facets//facet-nationality 
                    | $facets//facet-genre  
                    | $facets//facet-period
                    | $facets//facet-monarch_period
                    | $facets//facet-gender
                    | $facets//facet-result
                    | $facets//facet-event_source
                    | $facets//facet-event_type
                    | $facets//facet-event_significance
                    | $facets//facet-location
                             
                    let $name := $facet/text()
                    let $type := tokenize($facet/name(), "[-]")[2]
                    group by $type, $name
                      return 
                        <facet>
                          <type>{$type}</type>
                          <name>{$name}</name>
                        </facet>
                  } 
              </facets>
              <hits>
                { 
                  if ($documentType = "author" or $documentType = "event"  or $documentType = "embeddedEvent") then
                    for $instance in $hit
                      let $xpath := helper:customPathToNode($instance)
                      let $context := $instance//ancestor::*[@id][1]/@id
                        return 
                          <hit>                            
                            <xpath>{$xpath}</xpath>
                            <context>{$context}</context>
                          </hit> 
                    else
                      for $instance in $hit
                        let $xpath := helper:customPathToNode($instance)
                        let $context := $instance//ancestor::*[@id][1]/@id
                          return 
                            <hit>                            
                              <xpath>{$xpath}</xpath>
                              <context value="{$instance}"></context>
                            </hit> 
                    }
                    </hits>
                </document>
                                        
  return 
    <result>
    <main>
      {$allResults}
      </main>
    </result>
    
};


(:
Standard full text search
:)
declare function search:standardSearch($database, $searchTerms,  $stemming) {
  if ($stemming = "true") then (
    let $hits := ft:search($database, $searchTerms)
      return $hits
  )
  else (
    for $hit in ft:search($database, $searchTerms)
      where $hit contains text {$searchTerms}
        return $hit
  )

};

(:
Advanced search, that checks to see if hit occurs within a tag + [optional] attribute + [optional] attribute value
:)
declare function search:advancedSearch($database, $searchTerms,  $advanced, $stemming) {

   let $tag := map:get($advanced, "tag")
   let $attribute := xs:QName(map:get($advanced, "attribute"))
   let $attributeValue := xs:QName(map:get($advanced, "attributeValue"))

   let $hits := 
   if ($stemming = "true") then (
    if (exists($tag) and exists($attribute) and exists($attributeValue)) then (
      for $hit in ft:search($database, $searchTerms)
        where $hit/ancestor::*[name()=$tag][@*[node-name(.)=$attribute]=$attributeValue]
          return $hit 
    ) else if (exists($tag) and exists($attribute) and not(exists($attributeValue))) then (
      for $hit in ft:search($database, $searchTerms)
        where $hit/ancestor::*[name()=$tag][@*[node-name(.)=$attribute]]
          return $hit 
    ) else if (exists($tag) and not(exists($attribute)) and not(exists($attributeValue))) then (
        for $hit in ft:search($database, $searchTerms)
          where $hit/ancestor::*[name()=$tag]
            return $hit   
   )) 
   else(
    if (exists($tag) and exists($attribute) and exists($attributeValue)) then (
      for $hit in ft:search($database, $searchTerms)
        where $hit/ancestor::*[name()=$tag][@*[node-name(.)=$attribute]=$attributeValue] and $hit contains text {$searchTerms}
          return $hit 
    ) else if (exists($tag) and exists($attribute) and not(exists($attributeValue))) then (
      for $hit in ft:search($database, $searchTerms)
        where $hit/ancestor::*[name()=$tag][@*[node-name(.)=$attribute]] and $hit contains text {$searchTerms}
          return $hit 
    ) else if (exists($tag) and not(exists($attribute)) and not(exists($attributeValue))) then (
        for $hit in ft:search($database, $searchTerms)
          where $hit/ancestor::*[name()=$tag] and $hit contains text {$searchTerms}
            return $hit   
   ) 
   )
   return $hits

};

(:
Gets a list of hits from the database
Filters hits by the faceting information provided in the query
Each hit will also include all associated document facets
:)
declare function search:getDatabaseHits($database, $filters, $advanced)  {
  let $hits :=
       if ((map:size($advanced) = 0)) then (
        for $hit in search:standardDatabaseSearch($database)
          let $facets :=  $hit/facet[1]
          where(exists(facet:filter($facets, $filters, $database, $hit)))
            return $hit
        )
          else 
          (
          for $hit in search:advancedDatabaseSearch($database, $advanced)
            let $facets :=  fn:root($hit)/facet[1]
            where(exists(facet:filter($facets, $filters, $database, $hit)))
              return $hit
          )
   
  let $allResults :=    
      for $hit in $hits
        let $facets :=  fn:root($hit)/facet[1]
        let $uri := $facets/uri
        let $documentName := $facets/doc-name/text()
        let $documentType := $facets/facet-result/text()   
        let $startDate := date:getStartSortingDate($facets)
        let $endDate := date:getEndSortingDate($facets)
        group by $uri, $documentName, $documentType, $startDate, $endDate 
          return
            <document uri="{$uri}" count="{count($hit)}" documentName="{$documentName}" documentType="{$documentType}" start="{$startDate}" end="{$endDate}"> 
                        <facets>
                          {
                                for $facet in $facets//facet-nationality 
                                | $facets//facet-genre  
                                | $facets//facet-period
                                | $facets//facet-monarch_period
                                | $facets//facet-gender
                                | $facets//facet-result
                                | $facets//facet-event_source
                                | $facets//facet-event_type
                                | $facets//facet-event_significance
                                | $facets//facet-location
                                
                                    let $name := $facet/text()
                                    let $type := tokenize($facet/name(), "[-]")[2]
                                    group by $type, $name
                                        return 
                                            <facet>
                                                <type>{$type}</type>
                                                <name>{$name}</name>
                                            </facet>
                            } 
                        </facets>
                          <hits>
                { 
                  if ($documentType = "author" or $documentType = "event" or $documentType = "embeddedEvent") then
                    for $instance in $hit
                      let $xpath := helper:customPathToNode($instance)
                      let $context := search:getTagContext($instance)
                        return 
                          <hit>                            
                            <xpath>{$xpath}</xpath>
                            <context>{$context}</context>
                          </hit> 
                    else
                      for $instance in $hit
                        let $xpath := helper:customPathToNode($instance)
                          return 
                            <hit>                            
                              <xpath>{$xpath}</xpath>
                              <context></context>
                            </hit> 
                    }
                    </hits>
                    </document>
                                        
  return 
    <result>
    <main>
      {$allResults}
      </main>
    </result>
};

(:
Standard database search, returns all documents in database
:)
declare function search:standardDatabaseSearch($database) {
  for $hit in db:open($database)
    return $hit
};

(:
Advanced database search, that checks to see if a tag + [optional] attribute + [optional] attribute value exists in document
:)
declare function search:advancedDatabaseSearch($database, $advanced) {

   let $tag := map:get($advanced, "tag")
   let $attribute := xs:QName(map:get($advanced, "attribute"))
   let $attributeValue := xs:QName(map:get($advanced, "attributeValue"))

   let $hits := 
   if (exists($tag) and exists($attribute) and exists($attributeValue)) then (
     
     
      for $hit in db:open($database)//*[name()=$tag][@*[node-name(.)=$attribute]=$attributeValue]
     return $hit
     ) else if (exists($tag) and exists($attribute) and not(exists($attributeValue))) then (
           for $hit in db:open($database)//*[name()=$tag][@*[node-name(.)=$attribute]]
               return $hit 
   ) else if (exists($tag) and not(exists($attribute)) and not(exists($attributeValue))) then 
   (
     for $hit in db:open($database)//*[name()=$tag]
     return $hit 
     
   ) 
 
   return $hits

};

(:
If the tag has a paragraph, or a header or an event chronprose as a parent or ancestor, return that id
If not, return the first paragraph, or a header or event chronprose ID that is a descendent of the selected tag
:)
declare function search:getTagContext($hit) {
  let $ancestors := 
    for $hits in $hit//ancestor-or-self::*[name()="P" or name()="CHRONPROSE" or name()="HEADING"][1]/@id
      return $hits
      
 let $descendants := 
    for $hits in $hit//descendant-or-self::*[name()="P" or name()="CHRONPROSE" or name()="HEADING"][1]/@id

      return $hits
   return
      if (exists($ancestors)) then (
         $ancestors[1]
      ) else  (
         $descendants[1]/@id
      )
    
};

  


