

(:
-----------FACET FILTER MODULE------------
This is the module that narrows down documents based on facets
It allows you to recursively take a group of hits and reduce them by extracted facet values
------------------------------------------
:)

module namespace facet = 'org.basex.modules.facet';

import module namespace date = "org.basex.modules.date";
import module namespace helper = "org.basex.modules.helper";
import module namespace functx='http://www.functx.com';
import module namespace map = 'http://www.w3.org/2005/xpath-functions/map';
import module namespace db = 'http://basex.org/modules/db ';


(:
Function that recursively goes through facet map
Filters hits based on each facet parameter listed
:)
declare %public function facet:filter($hits, $facets, $database, $fulltextHit)  {
 
   if (empty(map:keys($facets))) then 
        for $hit in $hits
            return $hit 
    else (
        let $facet := map:keys($facets)[1]
        let $facetedResults := 
            if($facet) then 
                if($facet = 'nationality') then facet:facetNationalities($hits, map:get($facets, $facet))
                else if($facet = 'genre') then facet:facetGenres($hits, map:get($facets, $facet)) 
                else  if($facet = 'result') then facet:facetResultTypes($hits, map:get($facets, $facet))
                else  if($facet = 'event_source') then facet:facetEventSources($hits, map:get($facets, $facet))
                else  if($facet = 'event_type') then facet:facetEventTypes($hits, map:get($facets, $facet))
                else  if($facet = 'event_significance') then facet:facetEventSignificances($hits, map:get($facets, $facet))
                else  if($facet = 'gender') then facet:facetGenders($hits, map:get($facets, $facet))
                else  if($facet = 'period') then facet:facetPeriods($hits, map:get($facets, $facet), $database)
                else  if($facet = 'date') then facet:facetDates($hits, map:get($facets, $facet))
                else  if($facet = 'tagIncluded') then facet:facetTags($hits, map:get($facets, $facet), $fulltextHit, 'included')
                else  if($facet = 'tagExcluded') then facet:facetTags($hits, map:get($facets, $facet), $fulltextHit, 'excluded')
                else  if($facet = 'locationIncluded') then facet:facetLocations($hits, map:get($facets, $facet), 'included')
                else  if($facet = 'locationExcluded') then facet:facetLocations($hits, map:get($facets, $facet), 'excluded')
                else $hits
            else $hits
        
        let $remainingFacets :=
            try { map:remove($facets, $facet) }
            catch * {''}
        return
            if(exists(map:keys($remainingFacets))) then facet:filter($facetedResults, $remainingFacets, $database, $fulltextHit)
            else $facetedResults
        )
        
};

(:
Function to return all hits that contain specific nationalities in the extracted <facet> element
:)
declare %private function facet:facetNationalities($hits, $nationalities as item()*) {

    for $nationality in $nationalities 
        let $nationalityResults := 
            for $hit in $hits
                where ($hit/facet-nationality/text() = $nationality) 
                    return $hit
            return $nationalityResults          

};

(:
Function to return all hits that contain specific genres in the extracted <facet> element
:)

declare %private function facet:facetGenres($hits, $genres  as item()*) {

    for $genre in $genres 
        let $genreResults := 
            for $hit in $hits
                where ($hit/facet-genre/text() = $genre) 
                    return $hit
            return $genreResults 

};

(:
Function to return all hits that contain specific result types in the extracted <facet> element
:)

declare %private function facet:facetResultTypes($hits, $resultTypes as item()*) {

    for $type in $resultTypes 
        let $resultTypeResults := 
            for $hit in $hits
                where ($hit/facet-result/text() = $type) 
                    return $hit
            return $resultTypeResults 

};

declare %private function facet:facetEventSources($hits, $eventSources as item()*) {

    for $source in $eventSources
        let $eventSourceResults := 
            for $hit in $hits
                where ($hit/facet-event_source/text() = $source) 
                    return $hit
            return $eventSourceResults 
};

declare %private function facet:facetEventTypes($hits, $eventTypes as item()*) {

    for $type in $eventTypes 
        let $eventTypeResults := 
            for $hit in $hits
                where ($hit/facet-event_type/text() = $type) 
                    return $hit
            return $eventTypeResults 

};

declare %private function facet:facetEventSignificances($hits, $eventSignificances as item()*) {

    for $significance in $eventSignificances 
        let $eventSignificanceResults := 
            for $hit in $hits
                where ($hit/facet-event_significance/text() = $significance) 
                    return $hit
            return $eventSignificanceResults 

};

(:
Function to return all hits that contain specific genders in the extracted <facet> element
:)

declare %private function facet:facetGenders($hits, $genders) {

    for $gender in $genders
        let $genderResults := 
            for $hit in $hits
                where ($hit/facet-gender/text() = $gender) 
                    return $hit
            return $genderResults 

};

(:
Function to return all hits that contain specific periods in the extracted <facet> element
:)
declare %private function facet:facetPeriods($hits, $periods, $database) {
    
      for $period in $periods
            for $hit in $hits
                where (($hit/facet-period/text() = $period) or ($hit/facet-monarch_period/text() = $period)  or ($hit/facet-author_period/text() = $period)) 
                    return $hit  

};

(:
Function to return all hits that contain specific locations in the extracted <facet> element
:)
 declare %private function facet:facetLocations($hits, $locations, $relationship){
      
      
      if ($relationship = "included") then (
        for $location in $locations
            for $hit in $hits
                where ($hit/facet-location/text() = $location) 
                    return $hit)

        else if ($relationship = "excluded") then (
            for $location in $locations
                for $hit in $hits
                    let $locations := for $docLocation in $hit/facet-location/text()
                        where ($docLocation = $location)
                            return $location
                    where not(exists($locations))
                        return $hit)  
        
};
(:
Function that returns all hits where the start and end date in the extracted FACET element occur within a specified time frame

There are three cases that signify overlap:
- the events of the document start in the time period
- the events of the document end in the time period
- the events of the document begin before and end after the time period
:)

declare %private function facet:facetDates($hits, $dateRange) {

    let $startDate := map:get($dateRange, "start")
    let $endDate := map:get($dateRange, "end")
    
    let $dateResults := 
                for $hit in $hits
                return $hit[($hit/facet-start/text() >= $startDate and $hit/facet-start/text() <= $endDate) or ($hit/facet-end/text() >= $startDate and $hit/facet-end/text() <= $endDate) or ($hit/facet-start/text() <= $startDate and $hit/facet-end/text() >= $endDate)]
                      
        return $dateResults 

 };

(:
Will return hit if all tags occured on the path to the hit
:)
declare %private function facet:facetTags($hits, $tags, $fulltextHit, $relationship) {
   if ($relationship = "included") then (
   for $tag in $tags
            let $xpath  := $fulltextHit/ancestor-or-self::*/name(.)
                where exists(functx:value-intersect($xpath, $tag)) 
                    return $hits                  )
   else if ($relationship = "excluded") then (
      for $tag in $tags
            let $xpath  := $fulltextHit/ancestor-or-self::*/name(.)
                where not(exists(functx:value-intersect($xpath, $tag))) 
                    return $hits   
   )
            
};