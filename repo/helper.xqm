
(:
---------------HELPER MODULE-------------
This is the module that contains misc. helper functions
------------------------------------------
:)

module namespace helper = 'org.basex.modules.helper';

import module namespace functx='http://www.functx.com';

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace xlink = "http://www.w3.org/1999/xlink";
declare namespace orl = "http://ualberta.ca/orlando";
declare namespace mods = "http://www.loc.gov/mods/v3";


(:
Returns the type of a document based on the document uri
:)
declare %public function helper:getResultType($uri as xs:string) as xs:string { 

    if(boolean(doc($uri)/ENTRY)) then
        let $resultType := "author"
            return $resultType
    else if (boolean(doc($uri)/EVENT)) then
        let $resultType := "event"
            return $resultType
    else if (boolean(doc($uri)/EMBEDDEDEVENT)) then
        let $resultType := "embeddedEvent"
            return $resultType
    else if (boolean(doc($uri)/mods:mods)) then
        let $resultType := "title"
            return $resultType
     else if (boolean(doc($uri)/entity//organization)) then
        let $resultType := "organization"
            return $resultType
    else if (boolean(doc($uri)/entity//person)) then
        let $resultType := "person"
            return $resultType 
    else
        let $resultType := "null"
        return $resultType
};


(:
Attributes where the attribute value should be returned with the XPath
:)
declare variable $helper:SPECIAL_ATTRIBUTE_LIST := ("TITLE", "WROTEORPUBLISHEDAS", "MODE", "EROTIC", "RELATIONTO", 
"ISSUE", "AWARDTYPE", "POSITION", "SELF-DEFINED", "SOCIALRANK", "CURRENTALTERNATIVETERM", "FOREBEAR", "GENDER",
 "FAMILYBUSINESS", "COMPETENCE", "NAMECONNOTATION", "NAMESIGNIFIER", "NAMETYPE", "AUTHORNAMETYPE", "AUTHORSHIPCONTROVERSY", 
 "COLLABORATION", "CONTROVERSYDATE", "INFLUENCETYPE", "INVOLVEMENT", "DISPLACEMENT", "PUBLICATIONMODE", "MOTIVETYPE", 
 "TYPEOFNONSURVIVAL", "ACTIVISM", "MEMBERSHIP", "WOMAN-GENDERISSUE", "RELATIONSHIPTYPE", "FORMOFSERIALIZATION",
  "DESTROYEdatabaseY", "CENSORSHIP", "FORMALITY", "GENDEREDRESPONSE", "RESPONSETYPE", "INSTITUTION", "INSTITUTIONLEVEL",
   "RELIGIOUS", "STUDENTBODY", "PHILANTHROPYVOLUNTEER", "PROTAGONIST", "GENRENAME", "GENDEROFAUTHOR", "INTERTEXTTYPE", 
   "TITLETYPE", "SETTINGDATETYPE", "SETTINGCLASS", "SETTINGPLACETYPE", "TECHNIQUETYPE" );

(:
Returning XPath of similar format to
NODE/NODE/NODE[SPECIAL_ATTRIBUTE=ATTRIBUTE_VALUE][SPECIAL_ATTRIBUTE=ATTRIBUTE_VALUE]/NODE
:)
declare %public function helper:customPathToNode($nodes as node()*) as xs:string {

let $elements := $nodes/ancestor-or-self::*/name(.)
let $modifiedElements := 
    for $element in $elements
        let $attributes := 
            for $attribute in $nodes/ancestor-or-self::*[name()=$element]/@* 
                    where  ($attribute/name() = $helper:SPECIAL_ATTRIBUTE_LIST) 
                        return concat( "[", $attribute/name(), "=", $attribute, "]") 

            return  concat($element,  string-join($attributes, ""))

    return string-join($modifiedElements, "/")
 };

