(:
---------------DATE MODULE-------------
This is the module used to process Orlando dates
------------------------------------------
:)


module namespace date = 'org.basex.modules.date';
import module namespace functx ='http://www.functx.com';
import module namespace helper = "org.basex.modules.helper";


(: 
Function that gets the earliest possible correctly formatted date that an ambiguous date could be
- 1800 becomes 1800-01-01
- 1800-09 becomes 1800-09-01
:)
declare %public function date:getLowerDateRange($stringDate)  { 
    let $bceIdenticator := fn:substring($stringDate, 0, 2)
    let $date := tokenize(functx:trim($stringDate), '[-]')
    
    return (
        if ($bceIdenticator = '-') then (
            if (count($date) = 4) then 
                string-join($date, '-')  
            else if (count($date) = 3) then 
                concat(string-join($date, '-'), '-01') 
            else if (count($date) = 2) then 
                concat(string-join($date, '-'), '-01-01')
              ) else (
              if (count($date) = 3) then 
                string-join($date, '-')  
            else if (count($date) = 2) then 
                concat(string-join($date, '-'), '-01') 
            else if (count($date) = 1) then 
                concat($date, '-01-01')  
                
              )   
            )  

};


(: 
Function that gets the late possible correctly formatted date that an ambiguous date could be
- 1800 becomes 1800-12-31
- 1800-09 becomes 1800-09-30
:)
declare %public function date:getUpperDateRange($stringDate) { 
    let $bceIdenticator := fn:substring($stringDate, 0, 2) 
    let $date := tokenize(functx:trim($stringDate), '[-]')

    return (
        if ($bceIdenticator = '-') then (
                   if (count($date) = 4) then 
                        string-join($date, '-')     
                    else if (count($date) = 3) then 
                        xs:string(date:bce-last-day-of-month(concat(string-join($date, '-'), '-01') ))
                    else if (count($date) = 2) then 
                        xs:string(date:bce-last-day-of-month(concat(string-join($date, '-'), '-12-01'))) 
                      )
       else  (
         if (count($date) = 3) then 
                        string-join($date, '-')     
                    else if (count($date) = 2) then 
                        xs:string(functx:last-day-of-month(concat(string-join($date, '-'), '-01') ))
                    else if (count($date) = 1) then 
                        xs:string(functx:last-day-of-month(concat($date, '-12-01'))))  
         
       
        
     )
          
};

(:
Return date of the month without leap years for BCE
:)
declare %public  function date:bce-days-in-month
  ( $date as xs:anyAtomicType? )  as xs:integer? {
   (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    [month-from-date(xs:date($date))]
 } ;
 

 (:
 Return last day of a given month for bce dates
 :)
 declare %public function  date:bce-last-day-of-month( $date as xs:anyAtomicType? ) as xs:date {
  xs:date(
     concat(
       fn:substring($date, 0, 6) ,'-',
       functx:pad-integer-to-length(xs:integer(month-from-date(xs:date($date))),2),'-',
       functx:pad-integer-to-length(xs:integer(date:bce-days-in-month($date)),2)))
 } ;
 
(:
Extracts the birth of an author from an entry document
:)
declare %public function date:getBirthEstimate($uri) { 

    if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATE))then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATE)[1] 
            let $birthDate := $date/@VALUE/string() 
                return $birthDate
     else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATE)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate 
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
        else if (exists(doc($uri)//BIOGRAPHY//CHRONSTRUCT)) then 
            for $date in doc($uri)//BIOGRAPHY//CHRONSTRUCT//DATE[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate 
        else 
            let $birthDate := "9999-12-31" 
                return $birthDate
        };

(:
Extracts the death of an author from an entry document
If author is still living, assigns death to be 9999-12-31
:)
declare %public function date:getDeathEstimate($uri) { 
     
    if (exists(doc($uri)//BIOGRAPHY//DEATH//CHRONSTRUCT//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//CHRONSTRUCT//DATE)[1] 
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
     else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATE)[1] 
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATERANGE)[1] 
            let $deathDate := $date/@FROM/string() 
                return $deathDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE/DATESTRUCT)[1]
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
    else 
        let $deathDate := "9999-12-31"
            return $deathDate
};
  
  
declare %public function date:getStartSortingDate($facets) {
    if (exists($facets//facet-start)) then
      let $date := $facets//facet-start
      return $date
    else
    
      let $date := "9999-12-31"
      return $date
    
};

declare %public function date:getEndSortingDate($facets) {
      if (exists($facets//facet-end)) then
      let $date := $facets//facet-end
      return $date
    else
      let $date := "-9999-12-31"
      return $date
};
