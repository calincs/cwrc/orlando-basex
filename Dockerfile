FROM basex/basexhttp:9.3.2

ENV BASEX_ROOT_USER=admin \
  BASEX_ROOT_PASSWORD=admin \
  BASEX_USER=orlando \
  BASEX_DATABASE=orlando \
  BASEX_MAPPING_DATABASE=mapping \
  BASEX_PASSWORD=orlando \
  BASEX_JVM_MEMORY=-Xmx4g

USER root

COPY ./bin/init.sh /usr/local/bin/orlando_init.sh
COPY ./bin/basex /usr/local/bin/basex
COPY ./bin/basexhttp /usr/local/bin/basexhttp
COPY ./bin/basexserver /usr/local/bin/basexserver
COPY ./repo/ /srv/basex/repo/
COPY ./stopwords.txt /srv/basex/repo/
COPY .basex /srv/basex/

RUN set -ex; \
  chmod +x /usr/local/bin/orlando_init.sh 
RUN set -ex; \
  chmod +x /usr/local/bin/basex 
RUN set -ex; \
  chmod +x /usr/local/bin/basexhttp 
RUN set -ex; \
  chmod +x /usr/local/bin/basexserver
  
USER basex

CMD [ "orlando_init.sh" ]
