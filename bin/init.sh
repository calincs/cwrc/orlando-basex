#! /bin/sh -eux

# Starting the server in order to initialize default setup.
/usr/local/bin/basexserver -S

if /usr/local/bin/basexclient -V -U$BASEX_ROOT_USER -Padmin -c "LIST" || false; then
  /usr/local/bin/basexclient -V -U$BASEX_ROOT_USER -Padmin \
 -c "ALTER PASSWORD admin $BASEX_ROOT_PASSWORD; CHECK $BASEX_DATABASE; CHECK $BASEX_MAPPING_DATABASE" || true
 # Creating and granting permission to the default user.
 /usr/local/bin/basexclient -V -U$BASEX_ROOT_USER -P$BASEX_ROOT_PASSWORD \
 -c "CREATE USER $BASEX_USER $BASEX_PASSWORD; GRANT WRITE ON $BASEX_DATABASE TO $BASEX_USER; GRANT WRITE ON $BASEX_MAPPING_DATABASE TO $BASEX_USER" 
 # Installing all of the XQuery modules
  /usr/local/bin/basexclient -V -U$BASEX_ROOT_USER -Padmin \
  -c "REPO INSTALL /srv/basex/repo/functx-1.0.xar; REPO INSTALL /srv/basex/repo/helper.xqm; \
  REPO INSTALL /srv/basex/repo/date.xqm;  REPO INSTALL /srv/basex/repo/facet.xqm; REPO INSTALL /srv/basex/repo/search.xqm; REPO INSTALL /srv/basex/repo/extract.xqm;"
 
else
  echo "Well you have sthing to work with."
fi

# Stopping the server to let basexhttp do its things.
/usr/local/bin/basexserver stop

/usr/local/bin/basexhttp 
