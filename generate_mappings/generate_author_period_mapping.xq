(:
Extracts the birth of an author from an entry document
:)
declare %public function local:getBirthEstimate($uri) { 

    if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATE))then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATE)[1] 
            let $birthDate := $date/@VALUE/string() 
                return $birthDate
     else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATE)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate 
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//SHORTPROSE//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
        else if (exists(doc($uri)//BIOGRAPHY//CHRONSTRUCT)) then 
            for $date in doc($uri)//BIOGRAPHY//CHRONSTRUCT//DATE[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate 
        else 
            let $birthDate := "9999-12-31" 
                return $birthDate
        };

(:
Extracts the death of an author from an entry document
If author is still living, assigns death to be 9999-12-31
:)
declare %public function local:getDeathEstimate($uri) { 
     
    if (exists(doc($uri)//BIOGRAPHY//DEATH//CHRONSTRUCT//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//CHRONSTRUCT//DATE)[1] 
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
     else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATERANGE)[1] 
                let $birthDate := $date/@FROM/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//BIRTH//CHRONSTRUCT//DATESTRUCT)[1] 
                let $birthDate := $date/@VALUE/string() 
                    return $birthDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATE)[1] 
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATERANGE)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATERANGE)[1] 
            let $deathDate := $date/@FROM/string() 
                return $deathDate
    else if (exists(doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE//DATESTRUCT)) then 
        for $date in (doc($uri)//BIOGRAPHY//DEATH//SHORTPROSE/DATESTRUCT)[1]
            let $deathDate := $date/@VALUE/string() 
                return $deathDate
    else 
        let $deathDate := "9999-12-31"
            return $deathDate
};

let $db := "" (: REPLACE WITH DATABASE:)

for $entry in db:open($db)
  let $id := $entry/ENTRY/@ID/string()
  let $label := lower-case(fn:replace($entry//DIV0/STANDARD, 
",,+", ","))
  let $uri := base-uri($entry)
  let $birth := local:getBirthEstimate($uri)
  let $death := local:getDeathEstimate($uri)
  
  return
  <mapping category="author">
    <date>
      <start>{$birth}</start>
      <end>{$death}</end>
    </date>
    <label>{$label}</label>
    <machineName>{$id}</machineName>
  </mapping>