declare %private function local:getLocationFacetCount($uri)  {   

   let $placeAddresses := 
        for $address in doc($uri)//PLACE//ADDRESS 
            return 
                if(exists($address[@REG])) then (
                    $address/@REG/string()
                ) else (
                    $address/ADDRLINE/string())
                    
    let $placesWithRegAttribute := 
        for $place in doc($uri)//PLACE/PLACENAME  | doc($uri)//PLACE/SETTLEMENT | doc($uri)//PLACE/REGION | doc($uri)//PLACE/GEOG | doc($uri)//PLACE/AREA 
            return $place/@REG/string()

    let $placeOrlandoNames := 
        for $place in doc($uri)//PLACE
            
            let $placeName :=
                for $placeName in $place//PLACENAME
                    return
                        if(exists($placeName[@REG])) then (
                            $placeName/@REG/string()
                        ) else if(exists($placeName/text())) then ((
                            $placeName/string()))
            
            let $address :=
                for $address in $place//ADDRESS//ADDRLINE
                    return $address/string()
            
            let $settlement := 
                for $settlement in $place//SETTLEMENT
                    return
                        if(exists($settlement[@REG])) then (
                            $settlement/@REG/string()
                        ) else if(exists($settlement/text())) then (
                             $settlement/string())
            
            let $region :=
                for $region in $place//REGION
                    return
                        if(exists($region[@REG])) then (
                            $region/@REG/string()
                        ) else if(exists($region/text())) then (
                            $region/string())
            
            let $geog :=
                    for $geog in $place//GEOG
                        return
                            if(exists($geog[@REG])) then (
                                $geog/@REG/string()
                            ) else if(exists($geog/text())) then (
                                $geog/string())
            
            let $area :=
                    for $area in $place//AREA
                        return 
                            if(exists($area[@REG])) then (
                                $area/@REG/string()
                            )  else if(exists($area/text())) then (
                                $area/string())

            return string-join(($placeName, $address, $settlement, $region, $geog, $area), ", ")
            
    for $place in   (distinct-values($placesWithRegAttribute), distinct-values($placeAddresses), distinct-values($placeOrlandoNames))
    return $place

};

let $database := "" (: REPLACE THIS WITH THE NAME OF DATABASE:)

let $locations := for $hit in db:open($database)
  let $uri := base-uri($hit)
    return local:getLocationFacetCount($uri)
  
  
return
  <mappings>{    
    for $location in distinct-values($locations)
      return
      <mapping category="location">
         <names>
           <name>{$location}</name>
         </names>
         <label>{fn:lower-case($location)}</label>
         <machineName>{replace(replace(fn:lower-case($location), '\s+', '_'), ',','')}</machineName>
       </mapping>
}</mappings>