<?php

include('BaseXClient.php');

/**
 * BaseX Connection Class.
 */

class Connection  {

    
    private $session, $user, $password, $database;
    private $host = 'basex';  
    private $port = '1984';


    /**
     * Initialize Connection Class
     * 
     * @param string $DB
     * A string of the name of the database to connect to
     * @param string $user
     * A string of the user env variable e.g. "BASEX_ROOT_USER"
     * @param string $password
     * A string of the password env variable e.g. "BASEX_ROOT_PASSWORD"
     */
    public function __construct(string $DB, string $user, string $password) {

        $this->user = getenv($user); 
        $this->password = getenv($password); 

        $this->database = $DB;

        return $this;
    }

    /**
     * Start Session
     * 
     * Starts database session
     */
    public function startSession() {

        try {

            $this->session = new Session($this->host, $this->port, $this->user, $this->password);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Close Session
     * 
     *  Ends database session
     */
    public function closeSession() {

        $this->session->close();

        return $this;
    }

    /**
     * Open Database
     * 
     *  Opens database (uses the database that was initialized)
     */
    public function openDatabase()  {

        try {
            $this->session->execute("OPEN $this->database");
        } catch (\Exception $e) {
            throw new Exception("Failed to open database $this->database");
        }
        return $this;

    } 

    /**
     * Load File and Extract Facets
     * 
     * Loads XML based on file path, adds new file (or replaces previous file 
     * if a file of that name already exists). Uses extract module to update file
     * and insert <FACET> node at the start of the document with dacet data.
     * 
     * @param string $name
     * String name of the file
     * @param string $filepath
     * String path to the file to load
     */
    public function loadAndProcessFile(string $name, string $filepath)  { 
        
        $queryString = "";

        $xml = file_get_contents($filepath);

        $this->session->replace($name, $xml);

        $uri = "/$this->database/$name";

        
        $queryString .= "import module namespace extract = 'org.basex.modules.extract'; ";
        $queryString .= "let \$uri := '$uri' ";
        $queryString .= "return extract:extractFacetsForDatabase(\$uri)";  

        $extract = $this->getResults($queryString);

        return $this;

    }

    /**
     * Delete File
     * 
     * Deletes file from the database
     * @param string $name
     * A string name of the file to delete
     */
    public function deleteFile(string $name){

        try {
            $this->session->execute('delete ' . $name);    

        } catch (\Exception $e) {
            throw new Exception("Failed to delete file: $name.");
        }
        return $this;
    }
    
    /**
     * Index Database
     * 
     * Creates fulltext index neccessary for fulltext search
     */
    public function indexDatabase()  {

        try {
            $this->session->execute("CREATE INDEX FULLTEXT");
        } catch (\Exception $e) {
            throw new Exception("Failed to create fulltext index");
        }
        return $this;

    } 

    /**
     * Backup Database
     * 
     * Creates a zipped backup for the database, timestamped and stored in the database directory
     */
    public function backupDatabase()  {

        try {
            $this->session->execute("CREATE BACKUP $this->database");
            
        } catch (\Exception $e) {
            throw new Exception("Failed to create backup");

        }
        return $this;

    }

    /**
     * Restore Database
     * 
     * Restores database based on backups
     */
    public function restoreDatabase()  {

        try {
            $this->session->execute("RESTORE $this->database");
    
        } catch (\Exception $e) {
            throw new Exception("Failed to  restore database");
        }
        return $this;

    }

    /**
     * Get Query Results
     * 
     * Gets query results as an array
     * 
     * @param string $queryString
     * String xquery to be sent to BaseX
     */
    public function getResults(string $queryString): array { 

        $query = $this->session->query($queryString);
        
        // Get results
        try {
            $results = [];
            while ($query->more()) {
                $results[] = $query->next();
            }
        } catch (\Exception $e) {

            return ['error' => $e->getMessage()];
        }

        $query->close();

        return $results;
    }

}

?>
