<?php

/**
 * Some sample code to show how you could use the other classes
 * 
 */
include_once("Connection.php");
include_once("SearchQueryBuilder.php");  

$database = "orlando";
$user = "BASEX_ROOT_USER";
$password = "BASEX_ROOT_PASSWORD";

$connection = new Connection($database, $user, $password);
$connection->startSession();
$connection->openDatabase();

$path    = '/tmp/testData';
$files = array_diff(scandir($path), array('.', '..'));

foreach($files as $file){

    $connection->loadAndProcessFile($file, $path . "/" . $file);
}

$connection->indexDatabase();

$searchTerms = ["the"];
$searchFilters = array(
  "genres"  => array(
      "Adventure writing"
  )
);

$queryBuilder = new SearchQueryBuilder($database, $searchTerms);
$queryBuilder->setSearchFilters($searchFilters);
$queryString = $queryBuilder->buildSearchQuery();

$results = $connection->getResults($queryString);


foreach($results as $result) {
  print($result);
}

$connection->closeSession();

?>