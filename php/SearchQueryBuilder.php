<?php

//TODO: Sanitize filter data, Query bind question (ask Nia)
/**
 * Search Query Builder Class
 * 
 * Dynamically builds string XQuery search query
 */
class SearchQueryBuilder
{
    private $database, $searchTerms;
    private $searchFilters = null;
    public $query = '';

    // Set default paramters, expand to include other ones as needed
    private $legalModeValues = ["any", "any word", "all", "all words", "phrase"];
    private $mode = "any";

    /**
     * Initialize Class
     * 
     * Initialzies class and sanitizes input
     * 
     * @param string $database
     * String name of the database to query
     * @param array $searchTerms
     * Sequential array of the words to search
     */
    function __construct(string $database, array $searchTerms) {

        $this->database = $this->sanitizeInput($database);
        $this->searchTerms = $this->sanitizeInput($searchTerms);

      }
    
    /**
     * Set Search Filters
     * 
     * Call this function to set facets that will be applied to the search results
     * 
     * @param array $searchFilters
     * Associative array of arrays that represent the type of values of facets to apply
     * Keys can be: nationalities, genres, resultTypes, genders, periods, dates, tagsIncluded, locations
     *  Ex.
     * $searchFilters = array(
     * "genres"  => array("Narrative poetry", "Dialogue of the dead"),
     * "nationalities" => array("Chinese"),
     * "dates" => array("startDate" => "1800","endDate" => "1950")
     * );
     */
    public function setSearchFilters(array $searchFilters) {

        $this->searchFilters = $searchFilters;

        return $this;
    }

    /**
     * Set Options
     * 
     * @param string $mode
     * String search mode to be used
     * Values can be "any", "any word", "all", "all words", "phrase"
     */
    public function setOptions(string $mode) {

        if (in_array ($mode, $this->legalModeValues)) {
            $this->mode = $mode;
        }
            
        return $this;
    }
  
    /**
     * Build Search Query
     * 
     * Dynamically builds search XQuery based on what has been initialized and set
     */
    public function buildSearchQuery() {
        
        $this->query .= "import module namespace search = 'org.basex.modules.search'; ";
        $this->query .= "let \$DB := '$this->database' ";
        $this->query .= "let \$searchTerms := ('" . implode("','" , $this->searchTerms) . "') "; 
        if ($this->searchFilters) {
            $this->filtersString = '';
            foreach($this->searchFilters as $filter => $filterValues) { 
                // filters will be in sequential lists unless they are date values
                if (!$this->isAssocative($filterValues)) {
                   $this->filtersString .= "'$filter':('" . implode("','", $filterValues) . "') "; 
                } else {
                     $this->filtersString .= "'$filter': map {";
                     $tempList = array() ;
                    foreach($filterValues as $typeDate => $date) {
                       $tempList[] .= "'$typeDate':'$date'";

                    }
                    $this->filtersString .= implode(",", $tempList);

                }
            }
            $this->query .= "let \$filters := map{ $this->filtersString } ";
        } else {
            $this->query .= "let \$filters := map{ } ";
        }
        $this->query .= "let \$options := map {'mode':'$this->mode'} ";
        $this->query .= "return search:getSearchResults(\$DB, \$searchTerms, \$options, \$filters)";
        
        return $this->query;

    }

    /**
     * Sanitize Input
     * 
     * Removes any characters not whitelisted (i.e. alphanumerical) to prevent XML injection
     * @param $input
     */
    private function sanitizeInput($input) { //how to handle array of arrays
        // strip special characters here
        $sanitizedInput = preg_replace( '/[^a-zA-Z0-9_]/', '', $input);
        return $sanitizedInput;

    }

    /**
     * Check Array Type
     * 
     * Checks to see whether array is sequential or associative
     */
    private function isAssocative($array){
        if(array_keys($array) !== range(0, count($array) - 1)) {
            return true;
        } else {
            return false;
        }
    }
}

?>
